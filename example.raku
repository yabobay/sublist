use lib "lib";
use sublist;

say "The following line should say 3.\nIf it doesn't, please panic calmly.";
say "\t" ~ sublist::index(<d e f>, <a b c d e f g>);

say "The following lines should say 3 and 7.\nIf they don't, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
say "\t" ~ $_ for sublist::indices(<d e f>, <a b c d e f and d e f g>);
