unit module sublist;

our sub index(@a, @b --> Int) {
  indices(@a, @b, 1)[0] // Nil;
}

our sub indices(@a, @b, $limit = Inf) {
  return Nil if @a > @b;
  my @indices_ = @b.grep: @a[0], :k;
  my @indices;
  for @indices_ {
    @indices.push: $_ if @a eq @b[$_ ..^ $_+@a];
    last if @indices >= $limit;
  }
  return @indices.list or Nil;
}
