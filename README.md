sublist
=======

a sublist is when one list is found verbatim inside another list.

sublist::index
--------------

determines whether a list is a sublist of another. if it can, it
returns the location where it was found. otherwise, returns `Nil`.

```raku
use sublist;
say sublist::index(<d e f>, <a b c d e f g>); # should print 3
```
enjoy!!!

sublist::indices
----------------

finds all sublist indexes and returns a list of them. returns `Nil` if
none are found.

```raku
use sublist;
# ↓ should print 3 and 7
.say for sublist::indices(<d e f>, <a b c d e f and d e f g>);
```
